## Laravel 4.1 empty project boilerplate 

Empty Laravel project with [HTML5 Boilerplate](http://html5boilerplate.com/) and Twitter [Bootstrap](http://getbootstrap.com/).
## Install

Clone repo and run 

```
	composer update
```

## Template structure

header.blade.php holds the html head.
footer.blade.php holds the closing body, and html tag and also all the javascript.

template.blade.php includes both header and footer and takes the variable $viewToLoad which has the view that you want to load.

When you want to return a view you do :

```
return View::make('includes.template')->with(array(
	'viewToLoad' => 'hello'
	));
```